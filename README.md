# Sistemas Operacionais projeto final 2° Semestre

Compilar:

    Para compilar o programa no terminal do Linux basta acessar a pasta onde o programa esta usando o comando cd, depois utilizar o comando gcc nomeDoPrograma -o nomeDoPrograma.c.

Executar:
    
    Para executar é apenas necessário usar o comando ./nomeDoPrograma
    
Resultado: 
    
    O programa tem como objetivo zerar uma das contas através de tranferências que foram feitas de uma conta para outra. O programa zera com sucesso uma das contas e o resultado pode ser observado através dos prints na tela que vão aparecer durante a execução do programa.
