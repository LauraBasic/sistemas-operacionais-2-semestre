#define _GNU_SOURCE
#include <stdlib.h>
#include <malloc.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <sched.h>
#include <stdio.h>
//Lib pra usar o spleep
#include <unistd.h>
// 64kB stack
#define FIBER_STACK 1024*64

//Struct da conta
struct c {
    int saldo;
}; typedef struct c conta;
conta from, to;
int valor;
int flag;
// The child thread will execute this function
int transferencia( void *arg)
{
    if (from.saldo >= valor){
        from.saldo -= valor;
        to.saldo += valor;
        printf("\n****Transferência da conta From para conta To concluída com sucesso!****\n");
        //Enquanto a fag == 1 o programa vai continuar rodando, pois ainda é possível fazer transferências
        flag = 1;
       
    } else {
        printf("\n****Transferência nao foi concluída, saldo nao e suficiente!****\n");
        flag = 0;
    }
    printf("Saldo de c1: %d\n", from.saldo);
    printf("Saldo de c2: %d\n", to.saldo);
    return 0;
}
int main()
{
    void* stack;
    pid_t pid;
    int i;
    // Allocate the stack
    stack = malloc( FIBER_STACK );
        if ( stack == 0 ){
            perror("malloc: could not allocate stack");
            exit(1);
        }
    // Todas as contas começam com saldo 100
    from.saldo = 100;
    to.saldo = 100;
    printf( "Transferindo 10 para a conta c2\n" );
    valor = 10;
    for (i = 0; i <= 100; i++) {
        // Call the clone system call to create the child thread
        pid = clone( &transferencia, (char*) stack + FIBER_STACK,
        SIGCHLD | CLONE_FS | CLONE_FILES | CLONE_SIGHAND | CLONE_VM, 0 );
        if ( pid == -1 ){
            perror( "clone" );
            exit(2);
        }
        sleep(1);
        //printf("%d \n",i);
        if(flag != 1){
            //caso a conta from esteja zerada o programa vai parar
            break;
        }
    }
    // Free the stack
    free( stack );
    printf("Transferências concluídas e memória liberada.\n");
    return 0;
}